/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/13 21:53:09 by tmielcza          #+#    #+#             */
/*   Updated: 2014/06/02 01:55:39 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fcntl.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdlib.h>
#include "irc.h"
#include "libft.h"

static int	client_init(char *port, char *in)
{
	int							s;
	struct sockaddr_in			addr;

	if ((s = socket(PF_INET, SOCK_STREAM, getprotobyname("tcp")->p_proto)) <= 0)
		return (-1);
	addr.sin_family = AF_INET;
	addr.sin_port = htons(ft_atoi(port));
	addr.sin_addr.s_addr = inet_addr(in);
	if (connect(s, (const struct sockaddr *)&addr, sizeof(addr)))
	{
		return (-1);
	}
	return (s);
}

void		read_sock(int sock, int fd)
{
	char			buf[1025];
	int				ret;

	ft_bzero(buf, 1024);
	ret = recv(sock, buf, 1024, 0);
	write(fd, buf, ret);
}

void		read_stdin(int sock)
{
	char			buf[1025];
	int				ret;

	ft_bzero(buf, 1024);
	ret = read(0, buf, 1024);
	send(sock, buf, ret, 0);
}

int			main(int ac, char **av)
{
	int		sock;

	if (ac != 3)
		return (ft_putstr("Fuck\n"), -1);
	if ((sock = client_init(av[2], av[1])) < 0)
		return (ft_putstr("Client failed\n"), -1);
	while (1)
		just_a_select(sock);
	return (0);
	close(sock);
	return (0);
}
