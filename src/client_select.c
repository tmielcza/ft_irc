/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client_select.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/25 19:23:09 by tmielcza          #+#    #+#             */
/*   Updated: 2014/05/25 22:24:54 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/select.h>
#include <unistd.h>
#include <strings.h>
#include "irc.h"

void		just_a_select(int sock)
{
	struct timeval	tv;
	fd_set			fs[2];

	FD_ZERO(fs);
	FD_ZERO(fs + 1);
	FD_SET(0, fs);
	FD_SET(0, fs + 1);
	FD_SET(sock, fs + 1);
	FD_SET(sock, fs);
	tv.tv_sec = 0;
	tv.tv_usec = 0;
	texit(-1, select(sock, fs, fs + 1, NULL, &tv), "select", NULL);
	if (FD_ISSET(sock, fs))
		read_sock(sock, 0);
	if (FD_ISSET(0, fs))
		read_stdin(sock);
}
