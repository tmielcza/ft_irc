/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_opt.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/20 00:49:40 by tmielcza          #+#    #+#             */
/*   Updated: 2014/05/20 19:56:05 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>
#include "irc.h"
#include "libft.h"

void		get_opt(int ac, char **args, t_env *e)
{
	if (ac != 2)
	{
		printf(ERRMSG_USAGE, args[0]);
		exit(1);
	}
	e->progname = args[0];
	e->port = ft_atoi(args[1]);
}
