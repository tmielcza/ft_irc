/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   chan.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/22 22:26:27 by tmielcza          #+#    #+#             */
/*   Updated: 2014/05/25 23:21:28 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "irc.h"
#include "libft.h"

static t_chan	*new_chan(char *name, t_env *e)
{
	t_chan	*chan;

	chan = (t_chan *)texitv(NULL, malloc(sizeof(t_chan)), "malloc", e);
	ft_strncpy(chan->name, name, CHAN_MAX_LEN - 2);
	chan->name[CHAN_MAX_LEN - 1] = '\0';
	chan->users = NULL;
	chan->tail = NULL;
	chan->nusers = 0;
	return (chan);
}

t_chan			*find_chan(t_list **chans, char *name)
{
	t_list	**tmp;

	tmp = &chans[hash(name) % CHAN_TAB_SIZE];
	while (*tmp && ft_strcmp(((t_chan *)(*tmp)->content)->name, name))
		*tmp = (*tmp)->next;
	if (*tmp)
		return ((*tmp)->content);
	return (NULL);
}

t_chan			*create_chan(char *name, t_list **chans, t_env *e)
{
	t_list	**tmp;
	t_chan	*chan;

	tmp = &chans[hash(name) % CHAN_TAB_SIZE];
	chan = new_chan(name, e);
	while (*tmp && (*tmp)->next)
		*tmp = (*tmp)->next;
	if (!*tmp)
		*tmp = (t_list *)texitv(NULL, new_link(NULL, NULL, chan), "malloc", e);
	else
		(*tmp)->next = (t_list *)texitv(NULL, new_link(NULL, NULL, chan),
										"malloc", e);
	return (chan);
}

void			join_chan(t_chan *chan, t_user *usr, t_env *e)
{
	if (chan->tail)
	{
		chan->tail->next = (t_list *)texitv(NULL,
							new_link(chan->tail, NULL, usr), "malloc", e);
		chan->tail = chan->tail->next;
	}
	else
		chan->tail = (t_list *)texitv(NULL,
						new_link(chan->tail, NULL, usr), "malloc", e);
	if (!chan->users)
		chan->users = chan->tail;
	usr->chan = chan;
	usr->chan->nusers += 1;
}
