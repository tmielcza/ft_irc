/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/22 23:27:34 by tmielcza          #+#    #+#             */
/*   Updated: 2014/05/24 20:47:15 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"
#include "irc.h"

t_list		*new_link(t_list *prev, t_list *next, void *content)
{
	t_list	*link;

	link = (t_list *)malloc(sizeof(t_list));
	link->next = next;
	link->prev = prev;
	link->content = content;
	return (link);
}

void		del_link(t_list **link)
{
	if ((*link)->prev)
		(*link)->prev->next = (*link)->next;
	if ((*link)->next)
		(*link)->next->prev = (*link)->prev;
	free(*link);
}
