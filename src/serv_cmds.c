/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   serv_cmds.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/23 14:44:24 by tmielcza          #+#    #+#             */
/*   Updated: 2014/05/25 23:10:52 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "irc.h"
#include "libft.h"

void		serv_join(int s, char *av, t_env *e)
{
	t_chan	*chan;

	if (!(chan = find_chan(e->chans, av)))
		chan = create_chan(av, e->chans, e);
	join_chan(chan, e->fds[s].usr, e);
	printf("canal = [%s]\n", e->fds[s].usr->chan->name);
}

void		serv_nick(int s, char *av, t_env *e)
{
	t_user	*usr;

	if (!(usr = find_user(e->users, av)))
		change_nick(av, e->fds[s].usr, e);
	printf("nick = [%s]\n", e->fds[s].usr->nick);
}

void		serv_msg(int s, char *av, t_env *e)
{
	char	*recvr;
	t_user	*rec;

	recvr = av;
	if (!(av = (char *)memchr(av, ' ', 10)))
		return ;
	*av = '\0';
	printf("av = [%s]\n", recvr);
	if (!(rec = find_user(e->users, recvr)))
		return ;
	send_user(e->fds[s].usr, rec, av + 1);
}
