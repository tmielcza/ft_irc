/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_fd.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/22 02:24:08 by tmielcza          #+#    #+#             */
/*   Updated: 2014/05/22 03:28:50 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "irc.h"

void		init_fd(t_fd *fd)
{
	fd->read.end = fd->read.buf + BUF_SIZE - 1;
	fd->read.head = fd->read.buf;
	fd->read.tail = fd->read.buf;
	fd->read.size = 0;
	fd->write.end = fd->write.buf + BUF_SIZE - 1;
	fd->write.head = fd->write.buf;
	fd->write.tail = fd->write.buf;
	fd->write.size = 0;
}
