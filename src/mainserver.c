/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mainserver.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/20 15:31:35 by tmielcza          #+#    #+#             */
/*   Updated: 2014/05/23 23:39:10 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "irc.h"

int			main(int ac, char **av)
{
	t_env	e;

	get_opt(ac, av, &e);
	env_init(&e);
	server_init(&e);
	endless_loop_from_beyond(&e);
	return (0);
}
