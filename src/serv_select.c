/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   serv_select.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/21 00:30:55 by tmielcza          #+#    #+#             */
/*   Updated: 2014/05/25 23:27:33 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/select.h>
#include <unistd.h>
#include <strings.h>
#include "irc.h"

void		the_select(t_env *e)
{
	int				n;
	int				i;
	struct timeval	tv;
	fd_set			fs[2];
	static void		(*tab[])(t_env *e, int i) =

	{read_server, read_client, write_server, write_client};
	FD_COPY(&e->fd_read, fs);
	FD_COPY(&e->fd_write, fs + 1);
	tv.tv_sec = 0;
	tv.tv_usec = 0;
	n = texit(-1, select(e->fd_max + 1, fs, fs + 1, NULL, &tv), "select", e);
	i = 0;
	while (i <= e->fd_max && n > 0)
	{
		if (FD_ISSET(i, fs))
			tab[e->fds[i].type - 1](e, i);
		if (FD_ISSET(i, fs + 1))
			tab[e->fds[i].type + 1](e, i);
		if (FD_ISSET(i, fs) || FD_ISSET(i, fs + 1))
			--n;
		i++;
	}
}
