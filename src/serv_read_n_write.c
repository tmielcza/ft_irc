/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   serv_read_n_write.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/21 01:29:34 by tmielcza          #+#    #+#             */
/*   Updated: 2014/08/07 17:22:53 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/select.h>
#include "irc.h"
#include "libft.h"

void		read_server(t_env *e, int i)
{
	struct sockaddr_in		sin;
	socklen_t				sl;
	int						s;
	t_user					*usr;

	sl = sizeof(sin);
	s = texit(-1, accept(i, (struct sockaddr *)&sin, &sl), "accept", e);
	printf("New connection!\n");
	ft_bzero(&e->fds[s], sizeof(*e->fds));
	init_fd(&e->fds[s]);
	e->fds[s].type = FD_CL;
	FD_SET(s, &e->fd_read);
	e->fd_max = MAX(s, e->fd_max);
	usr = create_user(DEFAULT_NICK, e->users, e);
	usr->sock = &e->fds[s];
	e->fds[s].usr = usr;
	e->fds[s].fd = s;
	serv_join(s, DEFAULT_CHAN, e);
}

void		read_client(t_env *e, int i)
{
	int		r;
	char	buf[BUF_SIZE];

	ft_bzero(buf, BUF_SIZE);
	if ((r = recv(i, buf, BUF_SIZE, 0)) <= 0)
	{
		FD_CLR(i, &e->fd_read);
		serv_join(i, "DEADwerqwtwegr", e);
	}
	fill_buf(&e->fds[i].read, buf, r);
	if (ft_strnstr(buf, "\n", r))
		exec_cmd(&e->fds[i].read, i, e);
	(void)e;
}

void		write_server(t_env *e, int i)
{
	int		r;
	char	buf[1024];

	r = recv(i, buf, 1024, 0);
	write(1, buf, r);
	(void)e;
	(void)i;
}

void		write_client(t_env *e, int i)
{
	int		r;
	char	buf[1024];

	r = recv(i, buf, 1024, 0);
	write(1, buf, r);
	(void)e;
	(void)i;
}
