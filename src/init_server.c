/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_server.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/20 17:31:51 by tmielcza          #+#    #+#             */
/*   Updated: 2014/05/25 19:10:02 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <netinet/in.h>
#include <netdb.h>
#include <stdlib.h>
#include <sys/select.h>
#include "irc.h"

void		server_init(t_env *e)
{
	int							s;
	struct sockaddr_in			sin;
	struct protoent				*pe;

	pe = (struct protoent *)texitv(NULL,
		getprotobyname("tcp"), "getprotobyname", e);
	s = texit(-1, socket(PF_INET, SOCK_STREAM, pe->p_proto), "socket", e);
	sin.sin_family = AF_INET;
	sin.sin_port = htons(e->port);
	sin.sin_addr.s_addr = htonl(INADDR_ANY);
	texit(-1, bind(s, (const struct sockaddr *)&sin, sizeof(sin)), "bind", e);
	texit(-1, listen(s, MAX_CXS), "listen", e);
	FD_SET(s, &e->fd_read);
	e->fd_max = s;
	e->fds[s].type = FD_SV;
	create_chan(DEFAULT_CHAN, e->chans, e);
}
