/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/20 15:19:54 by tmielcza          #+#    #+#             */
/*   Updated: 2014/05/20 19:51:10 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "irc.h"

void		free_env(t_env *e)
{
	if (e->fds)
		free(e->fds);
	if (e->users)
		free(e->users);
	if (e->chans)
		free(e->chans);
}
