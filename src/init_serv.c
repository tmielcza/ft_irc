/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_serv.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/20 15:36:47 by tmielcza          #+#    #+#             */
/*   Updated: 2014/05/25 19:09:23 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/resource.h>
#include <sys/select.h>
#include <stdlib.h>
#include "irc.h"
#include "libft.h"

void		env_init(t_env *e)
{
	struct rlimit	rlp;

	e->fds = NULL;
	e->users = NULL;
	e->chans = NULL;
	texit(-1, getrlimit(RLIMIT_NOFILE, &rlp), "getrlimit", e);
	e->maxfds = rlp.rlim_cur;
	e->fds = (t_fd *)texitv(NULL,
				malloc(sizeof(*e->fds) * e->maxfds), "malloc", e);
	e->users = (t_list **)texitv(NULL,
				malloc(sizeof(*e->users) * USER_TAB_SIZE), "malloc", e);
	e->chans = (t_list **)texitv(NULL,
				malloc(sizeof(*e->chans) * CHAN_TAB_SIZE), "malloc", e);
	ft_bzero(e->fds, sizeof(e->fds) * e->maxfds);
	ft_bzero(e->users, sizeof(e->users) * e->maxfds);
	FD_ZERO(&e->fd_read);
	FD_ZERO(&e->fd_write);
}
