/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   user.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/23 16:30:06 by tmielcza          #+#    #+#             */
/*   Updated: 2014/05/25 03:08:53 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "irc.h"
#include "libft.h"

t_user			*find_user(t_list **users, char *name)
{
	t_list	**tmp;

	tmp = &users[hash(name) % USER_TAB_SIZE];
	while (*tmp && ft_strcmp(((t_user *)(*tmp)->content)->nick, name))
		*tmp = (*tmp)->next;
	if (*tmp)
		return ((*tmp)->content);
	return (NULL);
}

static t_user	*new_user(char *name, t_env *e)
{
	t_user	*user;

	user = (t_user *)texitv(NULL, malloc(sizeof(t_user)), "malloc", e);
	ft_strncpy(user->nick, name, NICK_SIZE - 2);
	user->nick[NICK_SIZE - 1] = '\0';
	user->sock = NULL;
	user->chan = NULL;
	user->flags = 0;
	return (user);
}

t_user			*create_user(char *name, t_list **users, t_env *e)
{
	t_list	**tmp;
	t_user	*user;
	t_list	*link;

	user = new_user(name, e);
	tmp = &users[hash(name) % USER_TAB_SIZE];
	while (*tmp && (*tmp)->next)
		*tmp = (*tmp)->next;
	link = (t_list *)texitv(NULL, new_link(*tmp, NULL, user), "malloc", e);
	if (!*tmp)
		*tmp = link;
	else
		(*tmp)->next = link;
	user->link = link;
	return (user);
}

void			change_nick(char *nick, t_user *usr, t_env *e)
{
	t_list		**tmp;
	t_list		*link;

	link = usr->link;
	ft_strncpy(usr->nick, nick, NICK_SIZE - 1);
	usr->nick[NICK_SIZE - 1] = '\0';
	if (link->prev)
		link->prev = link->next;
	if (link->next)
		link->next = link->prev;
	link->next = NULL;
	tmp = &e->users[hash(nick) % USER_TAB_SIZE];
	while (*tmp && (*tmp)->next)
		*tmp = (*tmp)->next;
	if (!*tmp)
		*tmp = link;
	else
	{
		(*tmp)->next = link;
		link->prev = *tmp;
	}
}
