/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   serv_exe.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/22 02:48:32 by tmielcza          #+#    #+#             */
/*   Updated: 2014/05/25 23:26:54 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "irc.h"
#include "libft.h"

#include <stdio.h>

static int	build_cmd(t_buf *buf, char *cmd)
{
	int		i;
	int		j;

	i = 0;
	j = 0;
	while ((i < 1 || cmd[i - 1] != '\n') && i < buf->size)
	{
		cmd[i] = buf->head[j];
		if (buf->head + j == buf->end)
		{
			buf->head = buf->buf;
			j = -1;
		}
		i++;
		j++;
	}
	buf->head += j;
	buf->size -= i;
	return (i);
}

void		exec_cmd(t_buf *buf, int s, t_env *e)
{
	int				i;
	char			cmd[BUF_SIZE];
	int				size;
	static t_cmd	tab[] = {

	{"/msg ", serv_msg},
	{"/nick ", serv_nick},
	{"/join ", serv_join},
	{NULL, NULL}
	};
	i = 0;
	size = build_cmd(buf, cmd);
	cmd[size - 1] = '\0';
	ft_putendl(cmd);
	while (tab[i].cmd && ft_strncmp(tab[i].name, cmd, ft_strlen(tab[i].name)))
		i++;
	if (tab[i].cmd)
		tab[i].cmd(s, cmd + ft_strlen(tab[i].name), e);
	else
		send_channel(e->fds[s].usr, cmd, size);
}
