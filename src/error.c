/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/20 01:04:00 by tmielcza          #+#    #+#             */
/*   Updated: 2014/05/25 22:24:30 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>
#include "libft.h"
#include "irc.h"

int			texit(int test, int ret, char *msg, t_env *e)
{
	if (test == ret)
	{
		printf("%s: %s error.\n", e->progname, msg);
		if (e)
			free_env(e);
		exit(1);
	}
	return (ret);
}

void		*texitv(void *test, void *ret, char *msg, t_env *e)
{
	if (test == ret)
	{
		printf("%s: %s error.\n", e->progname, msg);
		if (e)
			free_env(e);
		exit(1);
	}
	return (ret);
}

int			terror(int test, int ret, char *msg, t_buf *buf)
{
	if (test == ret)
		fill_buf(buf, msg, ft_strlen(msg));
	return (ret);
}
