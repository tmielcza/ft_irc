/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   send.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/24 20:01:50 by tmielcza          #+#    #+#             */
/*   Updated: 2014/05/25 23:28:08 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/socket.h>
#include "irc.h"

#include <stdio.h>
#include "libft.h"

/*
**	Remplacer 'send' par une vraie fonction...
*/
void		send_channel(t_user *usr, char *str, int size)
{
	t_list	*tmp;
	int		fd;

	tmp = usr->chan->users;
	fd = ((t_user *)tmp->content)->sock->fd;
	while (tmp)
	{
		if (tmp->content != usr)
		{
			send(fd, "<", 1, 0);
			send(fd, usr->nick, ft_strlen(usr->nick), 0);
			send(fd, "> ", 2, 0);
			send(fd, str, size, 0);
			send(fd, "\n", 1, 0);
		}
		tmp = tmp->next;
	}
}

void		send_user(t_user *usr, t_user *recvr, char *str)
{
	send(recvr->sock->fd, "*", 1, 0);
	send(recvr->sock->fd, usr->nick, ft_strlen(usr->nick), 0);
	send(recvr->sock->fd, "* ", 2, 0);
	send(recvr->sock->fd, str, ft_strlen(str), 0);
	send(recvr->sock->fd, "\n", 1, 0);
}
