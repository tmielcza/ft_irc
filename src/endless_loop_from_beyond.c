/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   endless_loop_from_beyond.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/20 23:27:58 by tmielcza          #+#    #+#             */
/*   Updated: 2014/05/21 00:29:23 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "irc.h"

void		endless_loop_from_beyond(t_env *e)
{
	while (1)
	{
		the_select(e);
	}
}
