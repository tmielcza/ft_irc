# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dmansour <dmansour@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/05/07 22:33:20 by dmansour          #+#    #+#              #
#    Updated: 2014/05/25 22:26:27 by tmielcza         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

LIB = -L./libft -lft
INC = -I./inc -I./libft/include
EXTRAFLAGS = -Wall -Wextra -Werror

CC = gcc
CFLAGS = $(EXTRAFLAGS) $(INC) $(LIB)
VPATH = src/

CLIENT = client
SERVEUR = serveur

SRCCLIENT =	client.c			\
			client_select.c		\
			error.c				\
			read.c				\
			free.c
SRCSERVER =	error.c				\
			free.c				\
			get_opt.c			\
			init_serv.c			\
			init_server.c		\
			mainserver.c		\
			serv_select.c		\
			endless_loop_from_beyond.c	\
			serv_read_n_write.c	\
			read.c				\
			init_fd.c			\
			serv_exe.c			\
			chan.c				\
			hash.c				\
			list.c				\
			serv_cmds.c			\
			user.c				\
			send.c

OBJCLIENT = $(SRCCLIENT:.c=.o)
OBJSERVER = $(SRCSERVER:.c=.o)

all: $(SERVEUR) $(CLIENT)

$(CLIENT): $(OBJCLIENT)
	make -C libft/
	$(CC) $(CFLAGS) -o $@ $^

$(SERVEUR): $(OBJSERVER)
	make -C libft/
	$(CC) $(CFLAGS) -o $@ $^

clean:
	rm -rf $(OBJCLIENT) $(OBJSERVER)
	make -C libft/ clean

fclean:
	rm -rf $(CLIENT) $(SERVEUR) $(OBJCLIENT) $(OBJSERVER)
	make -C libft/ fclean

re: fclean all
