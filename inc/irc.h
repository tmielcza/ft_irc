/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   irc.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/19 22:25:28 by tmielcza          #+#    #+#             */
/*   Updated: 2014/08/07 17:22:16 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef IRC_H
# define IRC_H

# include <sys/select.h>
# include <string.h>

# define NICK_SIZE		10
# define MSG_MAX_LEN	512
# define CHAN_MAX_LEN	200
# define CHAN_MAX_USR	100
# define MAX_CHAN		1000
# define CHAN_TAB_SIZE	256
# define USER_TAB_SIZE	256
# define BUF_SIZE		512
# define MAX_CXS		42

# define FD_ZO			0
# define FD_SV			1
# define FD_CL			2

# define MAX(a,b)		((a > b) ? a : b)
# define MIN(a,b)		((a < b) ? a : b)

# define STR_EOR		"\xFF\x1"
# define DEFAULT_CHAN	"Global"
# define DEFAULT_NICK	"BoB"
# define ERRMSG_USAGE	"Usage: %s <port>\n"

typedef struct		s_buf
{
	char			buf[BUF_SIZE];
	char			*end;
	char			*head;
	char			*tail;
	int				size;
}					t_buf;

typedef struct		s_list
{
	struct s_list	*next;
	struct s_list	*prev;
	void			*content;
}					t_list;

typedef struct		s_fd
{
	int				fd;
	int				type;
	t_buf			read;
	t_buf			write;
	struct s_user	*usr;
}					t_fd;

typedef struct		s_chan
{
	char			name[CHAN_MAX_LEN];
	t_list			*users;
	t_list			*tail;
	int				nusers;
}					t_chan;

typedef struct		s_user
{
	char			nick[NICK_SIZE];
	t_fd			*sock;
	int				flags;
	t_chan			*chan;
	t_list			*link;
}					t_user;

typedef struct		s_env
{
	char			*progname;
	int				port;
	int				maxfds;
	t_fd			*fds;
	t_list			**users;
	t_list			**chans;
	fd_set			fd_read;
	fd_set			fd_write;
	int				fd_max;
}					t_env;

typedef struct		s_cmd
{
	char			*name;
	void			(*cmd)(int s, char *av, t_env *e);
}					t_cmd;

void				get_opt(int ac, char **args, t_env *e);

int					texit(int test, int ret, char *msg, t_env *e);
void				*texitv(void *test, void *ret, char *msg, t_env *e);
int					terror(int test, int ret, char *msg, t_buf *buf);

void				free_env(t_env *e);
void				env_init(t_env *e);
void				server_init(t_env *e);
void				endless_loop_from_beyond(t_env *e);

/*
**	serv_select.c
*/
void				the_select(t_env *e);

void				read_server(t_env *e, int i);
void				read_client(t_env *e, int i);
void				write_server(t_env *e, int i);
void				write_client(t_env *e, int i);
void				fill_buf(t_buf *buf, char *str, int slen);
void				init_fd(t_fd *fd);
void				exec_cmd(t_buf *buf, int s, t_env *e);

/*
**	hash.c
*/
unsigned long		hash(char *str);

/*
**	chan.c (1 static)
*/
t_chan				*find_chan(t_list **chans, char *name);
void				join_chan(t_chan *chan, t_user *usr, t_env *e);
t_chan				*create_chan(char *name, t_list **chans, t_env *e);

/*
**	list.c
*/
t_list				*new_link(t_list *prev, t_list *next, void *content);
void				del_link(t_list **link);

/*
**	serv_cmds.c
*/
void				serv_join(int s, char *av, t_env *e);
void				serv_nick(int s, char *av, t_env *e);
void				serv_msg(int s, char *av, t_env *e);

/*
**	user.c (1 static)
*/
t_user				*find_user(t_list **users, char *name);
t_user				*create_user(char *name, t_list **users, t_env *e);
void				change_nick(char *nick, t_user *usr, t_env *e);

/*
**	send.c
*/
void				send_channel(t_user *usr, char *str, int size);
void				send_user(t_user *usr, t_user *recvr, char *str);

/*
**	client_select.c
*/
void				just_a_select(int sock);

/*
**	client.c (1 static)
*/
void				read_sock(int sock, int fd);
void				read_stdin(int sock);

#endif
