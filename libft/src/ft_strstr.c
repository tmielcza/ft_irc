/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 16:32:41 by tmielcza          #+#    #+#             */
/*   Updated: 2014/05/18 06:19:29 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <string.h>

char		*ft_strstr(const char *s1, const char *s2)
{
	int		j;
	int		i;

	if (*s2 == 0)
		return ((char *)s1);
	i = 0;
	j = 0;
	while (s1[i] != 0)
	{
		while (s1[i + j] && s1[i + j] == s2[j])
		{
			j++;
		}
		if (s2[j] == 0)
			return ((char *)s1 + i);
		j = 0;
		i++;
	}
	return (NULL);
}
