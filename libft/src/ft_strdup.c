/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 16:06:22 by tmielcza          #+#    #+#             */
/*   Updated: 2014/05/18 06:15:04 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <string.h>
#include "libft.h"

char		*ft_strdup(const char *s1)
{
	char	*ret;

	ret = ft_strnew(ft_strlen((char *)s1) + 1);
	if (ret)
		ft_memcpy(ret, s1, ft_strlen((char *)s1));
	return (ret);
}
