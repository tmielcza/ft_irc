/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 15:51:32 by tmielcza          #+#    #+#             */
/*   Updated: 2014/05/18 06:00:28 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

void		*ft_memchr(const void *s, int c, size_t n)
{
	unsigned char	*cha;

	if (!s)
		return (NULL);
	cha = (unsigned char *)s;
	while ((*cha++ != (unsigned char)c) && (n != 0))
		n--;
	if (cha[-1] == (unsigned char)c)
		return (cha - 1);
	return (NULL);
}
