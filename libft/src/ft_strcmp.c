/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 16:37:31 by tmielcza          #+#    #+#             */
/*   Updated: 2014/05/18 06:09:35 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <string.h>
#include "libft.h"

int			ft_strcmp(const char *s1, const char *s2)
{
	if (!*s2 || !*s1)
		return (*s1 - *s2);
	return (ft_memcmp(s1, s2, ft_strlen((char *)s2)));
}
