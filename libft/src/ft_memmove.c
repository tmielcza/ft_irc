/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/21 18:52:39 by tmielcza          #+#    #+#             */
/*   Updated: 2014/05/18 06:01:09 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include <libft.h>

void		*ft_memmove(void *dest, const void *src, size_t n)
{
	char	*tmp;
	char	*tmpd;
	int		i;

	tmpd = dest;
	i = 0;
	tmp = ft_memcpy(ft_strnew(n), src, n);
	while (i < (int)n)
	{
		tmpd[i] = tmp[i];
		i++;
	}
	if (tmp)
		return (dest);
	return (NULL);
}
