/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isdigit.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 16:53:14 by tmielcza          #+#    #+#             */
/*   Updated: 2014/01/17 19:05:20 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
int			ft_isdigit(int c)
{
	if (c >= '0' && c <= '9')
		return (42);
	return (0);
}
