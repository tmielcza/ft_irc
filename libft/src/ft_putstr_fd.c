/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/23 18:37:07 by tmielcza          #+#    #+#             */
/*   Updated: 2014/05/18 06:07:46 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include "libft.h"

void		ft_putstr_fd(char const *strbis, int fd)
{
	char	*str;

	str = (char *)strbis;
	while (*str)
	{
		ft_putchar_fd(*str, fd);
		str++;
	}
}
