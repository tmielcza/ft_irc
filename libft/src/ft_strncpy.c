/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/20 12:16:12 by tmielcza          #+#    #+#             */
/*   Updated: 2014/05/18 06:21:01 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <stdlib.h>
#include "libft.h"

char		*ft_strncpy(char *dest, const char *src, size_t n)
{
	char	*ret;

	if ((int)n <= (int)ft_strlen(src))
		ret = ft_memmove(dest, src, n);
	else
	{
		ret = ft_memmove(dest, src, ft_strlen(src));
		ft_bzero(dest + ft_strlen(src), n - ft_strlen(src));
	}
	if (ret)
		return (dest);
	return (NULL);
}
