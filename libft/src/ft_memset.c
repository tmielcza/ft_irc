/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/21 16:52:50 by tmielcza          #+#    #+#             */
/*   Updated: 2014/05/18 06:04:06 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <strings.h>

void		*ft_memset(void *b, int c, size_t len)
{
	size_t		i;
	char		*ptr;

	ptr = (char *)b;
	i = 0;
	while (i < len)
		ptr[i++] = (unsigned char)c;
	return (b);
}
