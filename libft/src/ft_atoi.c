/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 16:42:11 by tmielcza          #+#    #+#             */
/*   Updated: 2014/05/18 05:57:34 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <libft.h>

int			ft_atoi(const char *str)
{
	char	*strbis;
	int		sign;
	int		n;

	strbis = (char *)str;
	n = 0;
	while ((*strbis >= 9 && *strbis <= 13) || *strbis == 32)
		strbis++;
	if (*strbis == '-')
		sign = (-1);
	if (*strbis == '+' || ft_isdigit(*strbis))
		sign = 1;
	if (*strbis == '+' || *strbis == '-')
		strbis++;
	while (ft_isdigit(*strbis))
		n = n * 10 + (*strbis++ - '0');
	return (n * sign);
}
