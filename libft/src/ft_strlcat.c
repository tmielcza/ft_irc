/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcat.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 16:16:44 by tmielcza          #+#    #+#             */
/*   Updated: 2014/05/18 06:15:54 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <string.h>
#include "libft.h"

size_t		ft_strlcat(char *s1, const char *s2, size_t n)
{
	size_t	lens1;
	size_t	lens2;

	lens2 = ft_strlen((char *)s2);
	lens1 = ft_strlen(s1);
	if (n < lens1)
		return (n + lens2);
	ft_strncpy(s1 + lens1, s2, n - 2);
	s1[n - 1] = '\0';
	return (lens1 + lens2);
}
