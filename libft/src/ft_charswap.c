/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_charswap.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/18 05:58:41 by tmielcza          #+#    #+#             */
/*   Updated: 2014/05/18 06:35:08 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include "libft.h"

char		*ft_charswap(char *str, char c1, char c2)
{
	int		i;

	i = 0;
	while (str[i] && str[i] != c1)
		i++;
	if (!str[i])
		return (str);
	str[i] = c2;
	return (str + i + 1);
}
