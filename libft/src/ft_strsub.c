/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/23 14:47:14 by tmielcza          #+#    #+#             */
/*   Updated: 2014/05/18 06:19:38 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <stdlib.h>

char		*ft_strsub(char const *s, unsigned int start, size_t len)
{
	s = s;
	start = start;
	len = len;
	return ((char *)s);
}
