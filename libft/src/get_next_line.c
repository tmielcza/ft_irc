/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_next_line.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/03 18:18:29 by tmielcza          #+#    #+#             */
/*   Updated: 2014/05/18 06:34:12 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <gnl.h>
#include <libft.h>
#include <unistd.h>
#include <fcntl.h>

int			strjoininplace(char **s1, char *s2)
{
	char		*s;

	if (*s1 && s2)
	{
		if (!(s = ft_strnew(ft_strlen(*s1) + ft_strlen(s2) + 1)))
			return (1);
		ft_strcpy(s, *s1);
		ft_strcat(s, s2);
		*s1 = s;
		return (0);
	}
	return (1);
}

char		*charswap(char *str, int c1, int c2)
{
	char			*n;

	if (str && (n = ft_memchr(str, c1, ft_strlen(str))))
	{
		*n = (char)c2;
		return (n + 1);
	}
	return (NULL);
}
