/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   gnl.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/18 06:30:33 by tmielcza          #+#    #+#             */
/*   Updated: 2014/05/18 06:31:00 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#ifndef GNL_H
# define GNL_H

# define SIZE_BUFF 6
# define RETURN_ERROR return (-1)
# define RETURN_EOF return (0)

int			get_next_line(int const fd, char **line);

#endif
